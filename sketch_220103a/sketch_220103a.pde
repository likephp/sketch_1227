/**
 * Recursive Tree
 * by Daniel Shiffman.  
 * 
 * Renders a simple tree-like structure via recursion. 
 * The branching angle is calculated as a function of 
 * the horizontal mouse location. Move the mouse left
 * and right to change the angle.
 */
float[] positionX = new float[0]; 
float[] positionY = new float[0]; 
float[] thetas = new float[0];   

void setup() {
  size(640, 360);
}

void draw() {
  background(0);
  frameRate(30);
  stroke(255);
  for(int treeA = 0; treeA < thetas.length; treeA++) {
    tree(treeA);
  }
}

void tree(int index) {
  println(positionX[index]);
  println(positionY[index]);
  translate(positionX[index],positionY[index]);
  // Draw a line 120 pixels
  line(0,0,0,-120);
  // Move to the end of that line
  translate(0,-120);
  // Start the recursive branching!
  branch(60, thetas[index]);
}

void branch(float h,  float theta) {
  // Each branch will be 2/3rds the size of the previous one
  h *= 0.66;
  
  // All recursive functions must have an exit condition!!!!
  // Here, ours is when the length of the branch is 2 pixels or less
  if (h > 2) {
    pushMatrix();    // Save the current state of transformation (i.e. where are we now)
    rotate(theta);   // Rotate by theta
    line(0, 0, 0, -h);  // Draw the branch
    translate(0, -h); // Move to the end of the branch
    branch(h, theta);       // Ok, now call myself to draw two new branches!!
    popMatrix();     // Whenever we get back here, we "pop" in order to restore the previous matrix state
    
    // Repeat the same thing, only branch off to the "left" this time!
    pushMatrix();
    rotate(-theta);
    line(0, 0, 0, -h);
    translate(0, -h);
    branch(h, theta);
    popMatrix();
  }
}

void mouseClicked() {
  translate(0,0);
  addNewParticle();
}

void addNewParticle() {
  // Let's pick an angle 0 to 90 degrees based on the mouse position
  float a = (mouseX / (float) width) * 90f;
  //println(mouseX);
  // Convert it to radians
  thetas = append(thetas, radians(a));
  // Start the tree from the bottom of the screen
  positionX = append(positionX, mouseX);  
  positionY = append(positionY, mouseY);
}
