//const palette = chromotome.get().colors; //["#3B3247", "#006D8E", "#EEDCC4", "#B53021", "#421717"]
MyLine[] lines = new MyLine[0];
int linesNum = 1;

int DIST = 10;
int MAX;
int GEN = 10;

color stColor = color(204, 153, 0);

void setup() {
  size(displayWidth, displayHeight);
  //angleMode(DEGREES);

  MAX = width > height ? width : height;
  //stColor = random(palette);

  for (int i = 0; i < linesNum; i++) {
    lines = (MyLine[])append(lines, new MyLine());
  }
}

void draw() {
  background(235);

  noStroke();
  for (int i = 0; i < lines.length; i++) {
    pushMatrix();
    translate(width / 2, height / 2);
    rotate(360 * i / lines.length);
    lines[i].display();
    popMatrix();
  }

  fill(235);
  stroke(stColor);
  strokeWeight(10);
  circle(width / 2, height / 2, MAX * 0.2);
}

class MyLine {
  Obj[] objs = new Obj[0] ;
  float speed;
  float h;
  MyLine() {
    speed = random(3, 6);
    h = random(2, 3);
  }

  void display() {
    if (random(100) < GEN) {
      if ((objs.length == 0) || (objs.length > 0 && objs[objs.length - 1].hasDistance()) ) {
        objs = (Obj[])append(objs, new Obj(speed, h));
      }
    }

    for (int i = 0; i < objs.length; i++) {
      objs[i].move();
      objs[i].display();
    }

    if (objs.length > 0) {
      for (int j = objs.length - 1; j >= 0; j--) {
        //if (objs[j].isFinished()) {
        //  objs = (Obj[])splice(objs, objs[j], 1);
        //}
      }
    }
  }
}

class Obj {
    float x = 0;
    float y = 0;
    float speed;
    float w = random(10, 100);
    float h;
    color c;
  Obj(float tmpSpeed, float tmpH) {
    speed = tmpSpeed;
    h = tmpH;
    c = stColor;
  }

  void move() {
    x -= speed;
  }

  Boolean isFinished() {
    if (x < -MAX * 0.6 - w) {
      return true;
    } else {
      return false;
    }
  }

  Boolean hasDistance() {
    if (x < -(w + DIST)) {
      return true;
    } else {
      return false;
    }
  }

  void display() {
    fill(c);
    // rect(this.x, this.y, this.w, this.h, this.h / 2);
    ellipse(x, y, 10, 10);
  }
}
