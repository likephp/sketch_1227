float[] mass = new float[0];
float[] crown = new float[0];
float[] positionX = new float[0];
float[] positionY = new float[0];
float[] velocityX = new float[0];
float[] velocityY = new float[0];
PImage img;
PVector[] msk;

int counter = 0;
int counterBreak = 100;
int counterResetTimer = 200;
float sample = 20;

int colorType = 1;
// speed  0 ~ 50
int speed = 0;

float maxWidth = 0;
float maxHeight = 0;
float minWidth = 0;
float minHeight = 0;
/////////////////////////////////////////////////////////////////////////////////////////////////////

// Line 
MyLine[] lines = new MyLine[0];
int linesNum = 10;
int DIST = 10;
int MAX;
int GEN = 10;
color stColor = color(204, 153, 0);
Boolean clicked = false;
Boolean test = false;
float startWidth;
float startHeight;

void setup() {
  size(displayWidth, displayHeight);
  img=loadImage("map.jpg");
  msk = new PVector[0];
  startWidth = width / 2;
  startHeight = height /2;
 //image(img,0,0,displayWidth, displayHeight);
  
  for(int x=0; x<width; x+=sample){
    for(int y=0; y<height; y+=sample){
      msk = (PVector[]) append(msk, new PVector(x, y, 255));
    }
  }

  
  //Line init
    MAX = width > height ? width : height;
  //stColor = random(palette);

  for (int i = 0; i < linesNum; i++) {
    lines = (MyLine[])append(lines, new MyLine());
  }

}

/////////////////////////////////////////////////////////////////////////////////////////////////////

void draw() {

 background(0);

 image(img,0,0,displayWidth, displayHeight);
  for (int particleA = 0; particleA < mass.length; particleA++) {
    float accelerationX = 0;
    float accelerationY = 0;
    
    for (int particleB = 0; particleB < mass.length; particleB++) {
      if (particleA == particleB) {
        continue;
      }
      float distanceX = positionX[particleB] - positionX[particleA];
      float distanceY = positionY[particleB] - positionY[particleA];

      float distance = sqrt(distanceX * distanceX + distanceY * distanceY);
      if (distance < 1) {
        distance = 1;
      }

      float force = (distance - 320) * mass[particleB] / distance;
      accelerationX += force * distanceX;
      accelerationY += force * distanceY;
    }
    
    velocityX[particleA] = velocityX[particleA] * 0.99 + accelerationX * mass[particleA];
    velocityY[particleA] = velocityY[particleA] * 0.99 + accelerationY * mass[particleA];
  }
  
  for (int particle = 0; particle < mass.length; particle++) {
    if(particle == 0 ) {
      maxWidth = positionX[particle];
      minWidth = positionX[particle];
      maxHeight = positionY[particle];
      minHeight = positionY[particle];
    }
    //setLuDistncePosition(positionX[particle], positionY[particle]);
    positionX[particle] += velocityX[particle];
    positionY[particle] += velocityY[particle];
    if(colorType == 1) {
      fill(random(80,155),random(50,90),random(82,105));
    }
    noStroke();
    ellipse(positionX[particle], positionY[particle], mass[particle] *500, mass[particle] * 500);
    
    
    if(positionX[particle] > maxWidth) {
      maxWidth = positionX[particle];
    }
    if(positionX[particle] < minWidth) {
      minWidth = positionX[particle];
    }
    if(positionY[particle] > maxHeight) {
      maxHeight = positionY[particle];
    }
    if(positionY[particle] < minHeight) {
      minHeight = positionY[particle];
    }
    
  }
    println(maxWidth+":::"+ minWidth +":::"+maxHeight+":::"+ minHeight);
  //setLuDistncePosition(positionX, positionY);
  fill(222,87,111);
  //rect(minWidth + (maxWidth - minWidth) /2 , minHeight + (maxHeight - minHeight) / 2, 25, 25);
  startWidth = minWidth + (maxWidth - minWidth) /2;
  startHeight = minHeight + (maxHeight - minHeight) / 2;
  counter++;
  
  if(counter > 300 && !test) {
    counter = 0;
    //resetMass();
  }

// Line display
if(clicked && !test) {
  for (int i = 0; i < lines.length; i++) {
    pushMatrix();
    translate(startWidth, startHeight);
    rotate(360 * i / lines.length);
    lines[i].display();
    popMatrix();
  }
}

  for (int particle = 0; particle < mass.length; particle++) {
    println(particle +":["+positionX[particle]+"][" +positionY[particle] +"]");
  }  

  delay(speed);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////

void addNewParticle() {
  if(counter > counterBreak && !test) {
    return;
  }
  mass = append(mass,(random(0.003, 0.03)));
  createCrown();
  positionX = append(positionX, mouseX);  
  positionY = append(positionY, (mouseY));
  velocityX = append(velocityX, (0));
  velocityY = append(velocityY, (0));
  
  //controll line start
  clicked = true;
  startWidth = mouseX;
  startHeight = mouseY;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////

void mouseClicked() {
  //imageMode(CORNER);
  //image(img, 0, 0, displayWidth, displayHeight);
  addNewParticle();
}


/////////////////////////////////////////////////////////////////////////////////////////////////////

void mouseDragged() {
  addNewParticle();
}

void createCrown() {
  crown = append(crown,(random(0.003, 0.03)));  
}

void linePoint() {
  for (int i=0 ; i<100; i+=5) {
  // different strokeWeight
  strokeWeight(i);
  // different stroke color
  stroke(i*2.55);
  point(i*5, i*5);
  //println(i);
  }
}

void resetMass() {
 mass = new float[0];
 crown = new float[0];
 positionX = new float[0];
 positionY = new float[0];
 velocityX = new float[0];
 velocityY = new float[0];
 clicked = false;
}

// Line Class in below
class MyLine {
  Obj[] objs = new Obj[0] ;
  float speed;
  float h;
  MyLine() {
    speed = random(3, 6);
    h = random(2, 3);
  }

  void display() {
    if (random(100) < GEN) {
      if ((objs.length == 0) || (objs.length > 0 && objs[objs.length - 1].hasDistance()) ) {
        objs = (Obj[])append(objs, new Obj(speed, h));
      }
    }

    for (int i = 0; i < objs.length; i++) {
      objs[i].move();
      objs[i].display();
    }

    if (objs.length > 0) {
      for (int j = objs.length - 1; j >= 0; j--) {
        //if (objs[j].isFinished()) {
        //  objs = (Obj[])splice(objs, objs[j], 1);
        //}
      }
    }
  }
}

class Obj {
    float x = 0;
    float y = 0;
    float speed;
    float w = random(10, 100);
    float h;
    color c;
  Obj(float tmpSpeed, float tmpH) {
    speed = tmpSpeed;
    h = tmpH;
    c = stColor;
  }

  void move() {
    x -= speed;
  }

  Boolean isFinished() {
    if (x < -MAX * 0.6 - w) {
      return true;
    } else {
      return false;
    }
  }

  Boolean hasDistance() {
    if (x < -(w + DIST)) {
      return true;
    } else {
      return false;
    }
  }

  void display() {
    fill(c);
    // rect(this.x, this.y, this.w, this.h, this.h / 2);
    ellipse(x, y, 10, 10);
  }
}
